// npm dependencies
import { Inject, Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

/**
 * INTERCEPTOR :   Intercepts and handles an HttpRequest or HttpResponse.
 * @description    This class carries the logic of attaching the headers to http request .
 */
@Injectable()
export class CustomInterceptService implements HttpInterceptor {
  constructor(@Inject(Router) private router: Router) {}
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const headers: any = {
      'Content-Type': 'application/json',
    };
    const sessionData = JSON.parse(localStorage.getItem('sessionData'));
    if (sessionData) {
      headers.token = sessionData.token;
    }
    request = request.clone({
      setHeaders: headers,
    });

    return next.handle(request).pipe(
      tap(
        (event) => {
          console.log(event);
        },
        (error) => {
          if (error.status === 403) {
            // token required
          } else if (error.status === 401) {
            const loginPath = '/login';
            this.router.navigateByUrl(loginPath);
          }
        }
      )
    );
  }
}
