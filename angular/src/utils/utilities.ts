
let api_endpoint = 'http://localhost:8000/api';
export const buildURLstring = (url: string, ...params: Array<any>) => {
  params.forEach((param, index) => {
    const replacableParam = '$' + index;
    url = url.replace(replacableParam, param);
  });
  return api_endpoint + url;
};
