import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {
  constructor(private http: HttpClient) {}

  getService(options) {
    let methodType = options.method;
    let url = options.url;
    let data = options.data || {};
    switch (methodType) {
      case 'GET':
        return this.http.get(url).toPromise();
      case 'POST':
        return this.http.post(url, data).toPromise();
      case 'PUT':
        return this.http.put(url, data).toPromise();
      case 'DELETE':
        return this.http.delete(url, data).toPromise();
      default:
        break;
    }
  }
}
