import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceService } from './service.service';
import { buildURLstring } from '../utils/utilities';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild('loginForm') loginForm: NgForm;
  email: string = '';
  username: string = '';
  submitType: boolean = false;
  userData: Array<any> = [];
  constructor(private service: ServiceService) {}

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    const payload = {
      method: 'GET',
      url: buildURLstring('/users')
    };
    this.service.getService(payload).then(
      (data: any) => {
        this.userData = data.userData;
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  deleteUser(user) {
    const payload = {
      method: 'DELETE',
      url: buildURLstring('/users/$0', user.email)
    };
    this.service.getService(payload).then(
      (data: any) => {
        console.log(data);
        this.getAllUsers();
      },
      (error: any) => {
        console.log(error);
      }
    );
  }

  submitUser() {
    const payload = {
      method: !this.submitType ? 'POST' : 'PUT',
      url: buildURLstring('/users'),
      data: {
        name: this.username,
        email: this.email
      }
    };
    this.service.getService(payload).then(
      (data: any) => {
        alert(data.message);
        this.getAllUsers();
        this.username = '';
        this.email = '';
      },
      (error: any) => {
        alert(error.error.message);
      }
    );
  }

  addUser() {
    this.submitType = false;
    this.username = '';
    this.email = '';
  }

  updateUser(user) {
    this.submitType = true;
    this.username = user.name;
    this.email = user.email;
  }
}
