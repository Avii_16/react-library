import React from 'react';

export const SingleInput = (props) => {
    return <input
        className='form-control'
        type={props.type}
        name={props.name}
        value={props.value}
        defaultValue={props.defaultValue}
        placeholder={props.placeholder}
        disabled={props.disabled}
        maxLength={props.maxLength ? props.maxLength : '100'}
        onChange={props.controlFunc} />
}

export const Button = (props) => {
    return <button
        type={props.type}
        className="btn btn-secondary"
        onClick={props.controlFunc}>
        <b> {props.name}</b>
    </button>
}

export const ValidationMessage = (props) => {
    return <p>{props.message}</p>
}
