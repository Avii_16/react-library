import { doHttpCall } from '../../util/restapi';

export const fetchUsers = () => async dispatch => {
    const options = {
        method: 'GET',
        url: 'http://localhost:3001/employees'
    }
    const response = await doHttpCall(options);
    if (response) {
        dispatch({
            type: 'FETCH_USERS',
            payload: response.data
        })
    }
}

export const addUsers = (userDetails) => async dispatch => {
    const options = {
        method: 'POST',
        url: 'http://localhost:3001/employees',
        data: userDetails
    }
    const response = await doHttpCall(options);
    if (response) {
        dispatch({
            type: 'ADD_USER_SUCCESS_CASE',
            payload: response.data
        })
    }
}

export const deleteUser = (userId) => async dispatch => {
    const options = {
        method: 'DELETE',
        url: `http://localhost:3001/employees/${userId}`
    }
    const response = await doHttpCall(options);
    if (response) {
        dispatch({
            type: 'DELETE_USER_SUCCESS_CASE',
            payload: response.data
        })
    }
}