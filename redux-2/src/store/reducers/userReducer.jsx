const initialState = {
    userData: [],
    addUserStatus: false,
    deleteUserStatus: false
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case 'FETCH_USERS':
            return {
                ...state,
                userData: action.payload
            }
        case 'ADD_USER_SUCCESS_CASE':
            console.log(action.payload);
            return {
                ...state,
                addUserStatus: true
            }
        case 'DELETE_USER_SUCCESS_CASE':
            console.log(action.payload);
            return {
                ...state,
                deleteUserStatus: true
            }
        default:
            return state
    }
}