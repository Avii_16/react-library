import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchUsers, deleteUser } from '../store/actions/userAction';
import { Button } from '../shared/dependencies';

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.fetchUserAction();
    }

    deleteUser = (userId) => {
        this.props.deleteUserAction(userId);
        setTimeout(() => {
            this.props.fetchUserAction();
        }, 500)
    }

    render() {
        const { userList } = this.props;
        return (
            <div>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>age</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            (userList.length)
                                ? userList.map(user => {
                                    return <tr key={user.id}>
                                        <td>{user.employee_name}</td>
                                        <td>{user.employee_age}</td>
                                        <td>
                                            <Button
                                                type={"submit"}
                                                controlFunc={() => this.deleteUser(user.id)}
                                                name={"Delete"}
                                            />
                                            {/* <Button
                                                type={"submit"}
                                                controlFunc={() => this.updateUser(user)}
                                                name={"Update"}
                                            /> */}
                                        </td>
                                    </tr>
                                })
                                : null
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        userList: state.userRed.userData
    }
}

const dispatchActionToProps = (dispatch) => {
    return {
        fetchUserAction: () => dispatch(fetchUsers()),
        deleteUserAction: (id) => dispatch(deleteUser(id))
    }
}

export default connect(mapStateToProps, dispatchActionToProps)(Users);