import React, { Component } from 'react';
import { SingleInput, Button, ValidationMessage } from '../shared/dependencies';
import { fetchUsers, addUsers } from '../store/actions/userAction';
import { connect } from 'react-redux';

class addUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            age: ''
        }
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    addUserDetails = () => {
        const { name, age } = this.state;
        const data = {
            employee_name: name,
            employee_age: +age
        }
        this.props.addUserAction(data);
        this.props.fetchUserAction();
        this.setState({
            name: '',
            age: '',
        });
    }
    render() {
        const { name, age } = this.state;
        const { addUserStatus } = this.props;
        return (<div style={{ padding: 50 }}>
            {
                (addUserStatus)
                    ? <ValidationMessage message={'User added'} />
                    : null
            }
            <div style={{ padding: 10 }}>
                <p style={{ textAlign: "center" }}><b> Add User</b></p>
                <SingleInput
                    type={"text"}
                    name={"name"}
                    value={name}
                    controlFunc={this.handleChange}
                    placeholder={"Enter Name"}
                /> <br />
                <SingleInput
                    type={"number"}
                    name={"age"}
                    value={age}
                    controlFunc={this.handleChange}
                    placeholder={"Enter age"}
                /> <br />
                <Button
                    type={"submit"}
                    controlFunc={this.addUserDetails}
                    name={"Submit"}
                />
            </div>
        </div>);
    }
}


const mapStateToProps = (state) => {
    return {
        userList: state.userRed.userData,
        addUserStatus: state.userRed.addUserStatus
    }
}

const dispatchActionToProps = (dispatch) => {
    return {
        fetchUserAction: () => dispatch(fetchUsers()),
        addUserAction: (data) => dispatch(addUsers(data))
    }
}

export default connect(mapStateToProps, dispatchActionToProps)(addUser);