import React from 'react';
import './App.css';
import Users from './components/users';
import AddUser from './components/addUser';

function App() {
  return (
    <div className="row">
      <div className="col-sm-6">
        <Users />
      </div>
      <div className="col-sm-6">
        <AddUser />
      </div>
    </div>
  );
}

export default App;
