// npm dependencies
import React, { useState, useEffect } from 'react';

// Component dependencies
import UserTable from './tables/UserTable';
import AddUserForm from './forms/AddUserForm';
import EditUserForm from './forms/EditUserForm';

// Internal dependencies
import { STR } from './utilities/staticMessages';
import { buildURLstring, getService } from './utilities/util';

const App = () => {

  // Initializing the state.
  const initialUserState = { name: '', email: '' }

  // Setting state
  const [users, setUsers] = useState([])
  const [currentUser, setCurrentUser] = useState(initialUserState)
  const [editing, setEditing] = useState(false)

  useEffect(() => {
    getUsers();
  }, []);

  const getUsers = async () => {
    const payload = {
      method: 'GET',
      url: buildURLstring('/users')
    };
    const response = await getService(payload);
    if (response.status === 200) {
      setUsers(response.data.userData);
    } else {
      console.log(response.data.message);
    }
  }

  /**
   * @description    This method adds the user to users list. 
   * @param {Object} user It carries the user object.
   */
  const addUser = async (user) => {
    const payload = {
      method: 'POST',
      url: buildURLstring('/users'),
      data: user
    };
    const response = await getService(payload);
    if (response.status === 200) {
      alert(response.data.message);
      getUsers();
    } else {
      alert(response.data.message);
    }
  }

  /**
   * @description           This method deletes the user from users list. 
   * @param {String} email  It carries the user id.
   */
  const deleteUser = async (email) => {
    setEditing(false);
    const payload = {
      method: 'DELETE',
      url: buildURLstring('/users/$0', email)
    };
    const response = await getService(payload);
    if (response.status === 200) {
      alert(response.data.message);
      getUsers();
    } else {
      alert(response.data.message);
    }
  }

  /**
   * @description    This method updates the user in users list. 
   * @param {Object} user It carries the user object.
   */
  const updateUser = async (updatedUser) => {
    setEditing(false);
    const payload = {
      method: 'PUT',
      url: buildURLstring('/users'),
      data: updatedUser
    };
    const response = await getService(payload);
    if (response.status === 200) {
      alert(response.data.message);
      getUsers();
    } else {
      alert(response.data.message);
    }
  }

  /**
   * @description    This method enables the user to edit the details of selected user. 
   * @param {Object} user It carries the user object.
   */
  const editRow = user => {
    setEditing(true)
    setCurrentUser(user)
  }

  return (
    <div className="container">
      <h1>{STR.HEADING_LABEL}</h1>
      <div className="flex-row">
        <div className="flex-large">
          {
            (editing)
              ? <div>
                <h2>{STR.EDIT_USER_LABEL}</h2>
                <EditUserForm
                  editing={editing}
                  setEditing={setEditing}
                  currentUser={currentUser}
                  updateUser={updateUser} />
              </div>
              : <div>
                <h2>{STR.ADD_USER_LABEL}</h2>
                <AddUserForm addUser={addUser} />
              </div>
          }
        </div>
        <div className="flex-large">
          <h2>{STR.VIEW_USERS_LABEL}</h2>
          <UserTable
            users={users}
            editRow={editRow}
            deleteUser={deleteUser}
          />
        </div>
      </div>
    </div>
  )
}

// Default export
export default App