/**
 * @description This object contains all the static strings used.
 */
export const STR = {
    HEADING_LABEL     : 'CRUD App with Hooks',
    EDIT_USER_LABEL   : 'Edit user',
    ADD_USER_LABEL    : 'Add user',
    UPDATE_USER_LABEL : 'Update user',
    VIEW_USERS_LABEL  : 'View users',
    NAME_LABEL        : 'Name',
    EMAIL_LABEL       : 'Email',
    ACTIONS_LABEL     : 'Actions',
    EDIT_LABEL        : 'Edit',
    DELETE_LABEL      : 'Delete',
    NO_USERS_LABEL    : 'No users',
    CANCEL_LABEL      : 'Cancel'
}