import axios from 'axios';
export const getService = (options) => {
    try {
        return axios(options);
    } catch (e) {
        return e.response;
    }
}

export const buildURLstring = (url, ...params) => {
    const api_endpoint = 'http://localhost:8000/api';
    params.forEach((param, index) => {
        const replacableParam = '$' + index;
        url = url.replace(replacableParam, param);
    });
    return api_endpoint + url;
};