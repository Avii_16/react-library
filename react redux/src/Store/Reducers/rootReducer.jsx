// npm dependencies
import { combineReducers }          from 'redux';

//reducer imports
import usersReducer                 from './usersReducer';

/**
 * Combine all reducers into root reducer
 */
const rootReducer = combineReducers({
    usersReducer,
});

export default rootReducer;