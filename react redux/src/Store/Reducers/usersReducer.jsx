import * as allActions from '../Actions/actionConstants'

const initialState = {
    usersList: [],
    message: '',
    del_user_message: ''
}

function usersReducer(state = initialState, action) {
    switch (action.type) {
        // case allActions.FETCH_USERS:
        //     return action.payload;
        case allActions.RECIEVE_USERS:
            return {
                ...state,
                usersList: action.payload
            };
        case allActions.ADD_USER_STATUS:
            return {
                ...state,
                message: action.payload
            }
        case allActions.DELETE_USER_STATUS:
            return {
                ...state,
                del_user_message: action.payload
            }
        default:
            return state
    }
};

export default usersReducer;