import axios from 'axios';
export const getService = async (getOptions) => {
    try {
        let response = await axios(getOptions);
        return response;
    } catch (e) {
        return e.response;
    }
}