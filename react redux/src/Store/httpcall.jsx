// helper funtions for future use
// note : these functions are not used in the project


import request from 'superagent';

function _doHttpCall(request_object, data_object, callback) {
	request_object
		.send(data_object)
		.set('Accept', 'application/json')
		.set('Authorization', ('Bearer ' + localStorage.getItem('jwt-token')))
		.end((err, res) => {
			callback(err, res);
		});
}

const callbackToPromise = (asyncFunction, ...args) => {
	return new Promise((resolve, reject) => {
		asyncFunction(...args, (err, data) => {
			if (err) return reject(err);
			resolve(data);
		});
	});
};

function url(apiCall) {
	return (apiCall);
}

export function doHttpGet(full_url, callback) {
	_doHttpCall(request.get(full_url), {}, callback);
}

export const getHttpGetPromise = (relative_url) => {
	return callbackToPromise(doHttpGet, url(relative_url))
		.then(response => JSON.parse(response.text));
};