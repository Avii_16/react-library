import { getService } from '../restApi';
import * as allActions from '../Actions/actionConstants'
import * as userActions from '../Actions/usersActionCreators';

const baseUrl = 'http://localhost:8000/api';

const userService = () => next => async (action) => {
    let payloadData;
    let response;
    next(action)
    switch (action.type) {
        case allActions.FETCH_USERS:
            payloadData = {
                method: 'GET',
                url: baseUrl + '/users'
            };
            response = await getService(payloadData);
            if (response) {
                next(userActions.receiveUsers(response.data.userData));
            }
            break;
        case allActions.ADD_USER:
            payloadData = {
                method: 'POST',
                url: baseUrl + '/users/register',
                data: action.payload
            };
            response = await getService(payloadData);
            if (response) {
                next(userActions.addUserStatus(response.data.message));
            }
            break;
        case allActions.DELETE_USER:
            payloadData = {
                method: 'DELETE',
                url: baseUrl + '/users/' + action.payload
            };
            response = await getService(payloadData);
            if (response) {
                next(userActions.deleteUserStatus(response.data.message));
            }
            break;
        default:
            break;
    }
};

export default userService;