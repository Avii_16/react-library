import * as allActions from './actionConstants';

export function fetchUsers() {
    return {
        type: allActions.FETCH_USERS,
        payload: []
    };
}

export function receiveUsers(data) {
    return {
        type: allActions.RECIEVE_USERS,
        payload: data
    };
}

export function addUser(data) {
    return {
        type: allActions.ADD_USER,
        payload: data
    }
}

export function addUserStatus(data) {
    return {
        type: allActions.ADD_USER_STATUS,
        payload: data
    }
}

export function deleteUser(data) {
    return {
        type: allActions.DELETE_USER,
        payload: data
    }
}

export function deleteUserStatus(data) {
    return {
        type: allActions.DELETE_USER_STATUS,
        payload: data
    }
}
