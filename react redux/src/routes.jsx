// npm dependencies
import React, { Component } from "react";
import { BrowserRouter, Route } from "react-router-dom";

// all components for routing
import Home from './components/home';


/**
 * This is the routes component 
 */
class Routes extends Component {
  render() {
    return (
      <BrowserRouter>
        <React.Fragment>
          <Route exact path="/" component={Home} />
        </React.Fragment>
      </BrowserRouter>
    );
  }
}
export default Routes;