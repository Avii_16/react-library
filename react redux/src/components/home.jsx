import React, { Component } from 'react';
import Users from './users';
import AddUser from './addUser';

class Home extends Component {
    state = {}
    render() {
        return (
            <div className="row">
                <div className="col-sm-6"><Users /></div>
                <div className="col-sm-5"><AddUser /></div>
            </div>
        );
    }
}

export default Home;