import React, { Component } from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import { addUser, fetchUsers } from '../Store/Actions/usersActionCreators';
import { SingleInput, Button, ValidationMessage } from './sharedComponents';

class AddUser extends Component {

    constructor(props) {
        super(props);
        this.state = {
            name: '',
            email: ''
        }
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    addUserDetails = () => {
        const { name, email } = this.state;
        const data = {
            name: name,
            email: email
        }
        this.props.addUserAction(data);
        this.props.getUsersAction();
        this.setState({
            name: '',
            email: '',
        });
    }

    render() {
        const { name, email } = this.state;
        const { addUserStatus } = this.props;
        return (
            <div style={{ padding: 100 }}>
                {
                    (addUserStatus.length)
                        ? <ValidationMessage message={addUserStatus} />
                        : null
                }
                <div style={{ padding: 50 }}>
                    <p style={{ textAlign: "center" }}><b> Add User</b></p>
                    <SingleInput
                        type={"text"}
                        name={"name"}
                        value={name}
                        controlFunc={this.handleChange}
                        placeholder={"Enter Name"}
                    /> <br />
                    <SingleInput
                        type={"text"}
                        name={"email"}
                        value={email}
                        controlFunc={this.handleChange}
                        placeholder={"Enter Email"}
                    /> <br />
                    <Button
                        type={"submit"}
                        controlFunc={this.addUserDetails}
                        name={"Submit"}
                    />
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        addUserStatus: state.usersReducer.message || '',
        userData: state.usersReducer.usersList
    }
}

function mapDispatchToProps(dispatch) {
    return {
        addUserAction: bindActionCreators(addUser, dispatch),
        getUsersAction: bindActionCreators(fetchUsers, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(AddUser);