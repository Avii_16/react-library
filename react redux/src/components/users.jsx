import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import '../Assets/CSS/users.css'
import { fetchUsers, deleteUser } from '../Store/Actions/usersActionCreators';
import { Button, ValidationMessage } from './sharedComponents';

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }

    componentDidMount() {
        this.props.getUsersAction();
    }

    deleteUser = (user) => {
        this.props.deleteUserAction(user.email);
        setTimeout(() => {
            this.props.getUsersAction();
        }, 1000)
    }

    render() {
        const { userData, deleteUserStatus } = this.props;
        return (
            <div style={{ padding: 100 }}>
                <table id="users">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th></th>
                        </tr>
                        {
                            (userData && userData.length)
                                ? userData.map((user, index) => {
                                    return <tr key={index}>
                                        <td>{user.name}</td>
                                        <td>{user.email}</td>
                                        <td>
                                            <Button
                                                type={"submit"}
                                                controlFunc={() => this.deleteUser(user)}
                                                name={"Delete"}
                                            />
                                            <Button
                                                type={"submit"}
                                                controlFunc={() => this.updateUser(user)}
                                                name={"Update"}
                                            />
                                        </td>
                                    </tr>
                                })
                                : <tr>
                                    <td colSpan="4" style={{ textAlign: "center" }}>No data</td>
                                </tr>
                        }
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        userData: state.usersReducer.usersList,
        deleteUserStatus: state.usersReducer.del_user_message || ''
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getUsersAction: bindActionCreators(fetchUsers, dispatch),
        deleteUserAction: bindActionCreators(deleteUser, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Users);